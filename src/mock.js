const videos = [
  {
    id: 0,
    src: require("./assets/videos/BigBuckBunny.mp4"),
    poster: require("./assets/images/thumbnails/BigBuckBunny.jpg"),
    tag: "consectetur adipisicing",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit."
  },
  {
    id: 1,
    src: require("./assets/videos/ElephantsDream.mp4"),
    poster: require("./assets/images/thumbnails/ElephantsDream.jpg"),
    tag: "perspiciatis praesentium",
    description:
      "A consequuntur excepturi explicabo inventore, iste iure, nobis obcaecati odit pariatur quos vel voluptatibus."
  },
  {
    id: 2,
    src: require("./assets/videos/TearsOfSteel.mp4"),
    poster: require("./assets/images/thumbnails/TearsOfSteel.jpg"),
    tag: "labore nisi perferendis",
    description:
      "Aliquam corporis cumque delectus ducimus illo laudantium minus nemo, odit quisquam soluta sunt, tempora tenetur totam voluptates voluptatum."
  },
  {
    id: 3,
    src: require("./assets/videos/SubaruOutbackOnStreetAndDirt.mp4"),
    poster: require("./assets/images/thumbnails/SubaruOutbackOnStreetAndDirt.jpg"),
    tag: "ducimus eaque",
    description: "Dolorem ipsum molestias sint."
  },
  {
    id: 4,
    src: require("./assets/videos/Sintel.mp4"),
    poster: require("./assets/images/thumbnails/Sintel.jpg"),
    tag: "aperiam deserunt",
    description:
      "Amet aperiam consectetur cupiditate maxime non qui quo quod sapiente similique, sint."
  }
];

export default videos;
